#include <stdio.h>
#include <stdlib.h>


// creation of the matrix, of sizes m1x *  m2y
int **matrixCreate(int m1x , int m2y){
	int **resu = malloc(m1x * sizeof(int*));
	for(int i = 0 ; i  < m1x ; i++){
		int *aux = malloc(sizeof(int) * m2y);
		for(int j = 0 ; j < m2y ; ++j){
			aux[j] = 0;
		}
		resu[i] = aux;
	}
	return resu;
}

// multplication of matrix
int **multMatrix(int ** matrix1, int m1x , int m1y , int **matrix2 , int m2x , int m2y){
	if(m1y != m2x){
		return NULL;
	}
	int ** resu = matrixCreate(m1x , m2y);
	for(int i  = 0  ; i  < m1x ; i++){
		int *row = resu[i];
		int *row_matrix = matrix1[i];
		for(int k  = 0  ; k  < m2x ; k++){
			int val = row_matrix[k];
			int *row2 = matrix2[k];
			for(int j  = 0; j  < m2y ; j+=1){
				row[j] += val * row2[j];
			}
		}
	}
	return resu;
}

void freeMatrix(int ** matrix , int mx){
	for(int i  = 0  ; i  < mx ; i++){
		free(matrix[i]);
	}
	free(matrix);
}

// assign random numbers to the matrix
void matrixRandom(int **matrix , int m1x , int m1y){
	for(int i  = 0  ; i  < m1x ; i++){
		int *aux = matrix[i];
		for(int j  = 0  ; j  < m1y ; j++){
			aux[j] = rand() % 10;
		}
	}
}


void printMatrix(int **matrix , int mx , int my){
	for(int i  = 0  ; i  < mx ; i++){
		for(int j  = 0  ; j  < my ; j++){
			printf("%d " , matrix[i][j]);
		}
		printf("\n");
	}
}


int main (int argc , char* argv[]){
	int size = atoi(argv[1]);
	int **matrix1 = matrixCreate(size , size);
	int **matrix2 = matrixCreate(size , size);
	matrixRandom(matrix1 , size ,size);
	matrixRandom(matrix2 , size ,size);
	int **matrix3 = multMatrix(matrix1 , size ,size , matrix2 , size ,size);
	//printMatrix(matrix3 , size , size);
	freeMatrix(matrix1 , size);
	freeMatrix(matrix2 , size);
	freeMatrix(matrix3 , size);
	return 0;
}
