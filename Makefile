all: vec_oti avx_cod vec


vec_oti: code.c
	gcc -ftree-vectorize -ffast-math -march=native -fopt-info -o vec_oti code.c

avx_cod: vectorized_code.c
	gcc -mavx2 -g -o avx_cod vectorized_code.c

vec: code.c
	gcc -o vec code.c

clean:
	rm vec_oti vec avx_cod
