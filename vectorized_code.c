#include <stdio.h>
#include <stdlib.h>
#include <x86intrin.h>
#include <stdint.h>
#include <stddef.h>


// creation of the matrix, of sizes m1x *  m2y
int **matrixCreate(int m1x , int m2y){
	int **resu = malloc(m1x * sizeof(int*));
	for(int i = 0 ; i  < m1x ; i++){
		// allocate a memory region align to 32 bytes
		int *aux = aligned_alloc(32 , sizeof(int) * m2y);
		for(int j = 0 ; j < m2y ; ++j){
			aux[j] = 0;
		}
		resu[i] = aux;
	}
	return resu;
}


int **multMatrix(int ** matrix1, int m1x , int m1y , int **matrix2 , int m2x , int m2y){
	if(m1y != m2x){
		return NULL;
	}
	int ** resu = matrixCreate(m1x , m2y);
	for(int i  = 0  ; i  < m1x ; i++){
		int *row = resu[i];
		int *row_matrix = matrix1[i];
		for(int k  = 0  ; k  < m2x ; k++){
			int val = row_matrix[k];
			int *row2 = matrix2[k];
			int j;
			// create a __m256i with the same elements in all positions
			int aux[8] __attribute__((aligned(32))) = {val ,val ,val ,val ,val ,val ,val ,val };
			__m256i vec_b = _mm256_load_si256((__m256i*)aux);
			int size_m2y = m2y-8;
			for(j  = 0; j  < size_m2y ; j+=8){
				__m256i vec_a = _mm256_load_si256((__m256i*)(row2 + j));
				__m256i vec_c = _mm256_load_si256((__m256i*)(row+j));
				__m256i vec_d = _mm256_mullo_epi32(vec_a,vec_b);
				vec_d = _mm256_add_epi32(vec_d , vec_c);
				_mm256_store_si256((__m256i*)(row+j) , vec_d);
			}
			for(; j < m2y ; j++){
				row[j] += val * row2[j];
			}
		}
	}
	return resu;
}

void matrixRandom(int **matrix , int m1x , int m1y){
	for(int i  = 0  ; i  < m1x ; i++){
		int *aux = matrix[i];
		for(int j  = 0  ; j  < m1y ; j++){
			aux[j] = rand() % 10;
		}
	}
}

void freeMatrix(int ** matrix , int mx){
	for(int i  = 0  ; i  < mx ; i++){
		free(matrix[i]);
	}
	free(matrix);
}

void printMatrix(int **matrix , int mx , int my){
	for(int i  = 0  ; i  < mx ; i++){
		for(int j  = 0  ; j  < my ; j++){
			printf("%d " , matrix[i][j]);
		}
		printf("\n");
	}
}

int main (int argc , char* argv[]){
	int size = atoi(argv[1]);
	int **matrix1 = matrixCreate(size , size);
	int **matrix2 = matrixCreate(size , size);
	matrixRandom(matrix1 , size ,size);
	matrixRandom(matrix2 , size ,size);
	int **matrix3 = multMatrix(matrix1 , size ,size , matrix2 , size ,size);
	//printMatrix(matrix3 , size , size);
	freeMatrix(matrix1 , size);
	freeMatrix(matrix2 , size);
	freeMatrix(matrix3 , size);
	return 0;

}
